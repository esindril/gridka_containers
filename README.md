# Linux Containers and Docker Tutorial

This tutorial will guide you through the steps of setting up a set of containers to do some really simple physics analysis. Furthermore, during this tutorial you will grow to understand different concepts related to container technology. All the steps and commands that follow assume you are using a **CentOS7** operating system.

## [Some conventions](#conventions)
We will use the following convention when executing commands:
* `>` with root privileges - use **sudo**
* `~` with normal user privileges

Throughout the tutorial, we will refer to several entities:
* The **Docker host** is the inital machine that we start with, on which the **Docker daemon** runs.
* The **Docker client** is used when executing a particular command and interacting with the **Docker daemon**.

## [Getting Docker](#getting_docker)

**Docker** can be installed using the default package manager for **CentOS7**.

```bash
# Install the docker package
> yum install -y docker
```

One can check, start or stop the **Docker daemon** using the following commands:

```bash
# Check status of the daemon
> systemctl status docker

# Start daemon
> systemctl start docker

# Stop daemon
> systemctl stop docker
```

Another useful command to inspect the log file of the **Docker daemon** is:

```bash
> journalctl -xn --unit=docker
```

## [Docker basics](#docker_basics)
Docker is a client-server application. Both the **client** and the **server** use the same binary `/usr/bin/docker`. The difference comes from the flags that are passed when the binary is executed. You should consult the [official Docker CLI](https://docs.docker.com/engine/reference/commandline/cli/) from time to time when you are searching for a particular command and try to look at the parameters it accepts before executing it, using the **--help** flag.

The **Docker daemon** runs with root privileges and uses by default a **unix socket** for the communication between the server and the clients. Therefore, when you run docker commands you need to have root privileges - put **sudo** in front of every docker client command that you execute. If you get `docker: command not found` or something like `/var/lib/docker/repositories: permission denied` you may have an incomplete Docker installation or insufficient privileges to access the **Docker daemon** on your machine.

The **Docker daemon** can also bind on a TCP socket with TLS enabled so that it can be contacted over the network, but for the purpose of this tutorial we will have the **Docker daemon** running on a **unix socket**.

### Getting docker images

Use the **docker images** command to list the currently available Docker images. Images are used as the root file system (/) of containers. For example, an image can contain a Fedora operating system with a web server or a web application. Images contain the necessary binaries, libraries and configuration files that are required for a certain application to run. This collection of files represents the single fundamental difference between different Linux distributions or versions of the same distribution. Since we started out fresh, there should be no images stored with the **Docker daemon**:

```bash
~ sudo docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             VIRTUAL SIZE
```

Docker images are distributed using the [Docker Hub](https://registry.hub.docker.com/search?q=library) which is for Docker images what **GitHub** is for git repositories: namely a centralised place that facilitates the collaboration and easy sharing of "recipes". Using the **Docker client** one can search and pull **Docker images** from the **Docker registry (Hub)** which can then be used to build containers. Newly downloaded **Docker images** can also be used as a starting point for creating new images.

At this point we can pull an image from the **Docker Hub** and start our first container. We will start with a basic **Fedora 22** Docker image.

```bash
# Search for Docker images that contain "fedora" in their name
> docker search fedora

NAME                                   DESCRIPTION                                     STARS     OFFICIAL   AUTOMATED
fedora                                 Official Fedora 21 base image and semi-off...   179       [OK]
fedora/couchdb                                                                         31                   [OK]
fedora/apache                                                                          30                   [OK]
fedora/ssh                                                                             19                   [OK]
fedora/firefox                                                                         18                   [OK]
fedora/memcached                                                                       18                   [OK]
fedora/nginx                                                                           18                   [OK]
fedora/earthquake                                                                      17                   [OK]
vbatts/fedora-varnish                  https://github.com/vbatts/laughing-octo/tr...   2                    [OK]
dockingbay/fedora-rust                 Trusted build of Rust programming language...   2                    [OK]
fedora/owncloud                                                                        2                    [OK]
...
```

As you can see there are many **Docker images** that contain the **fedora** keyword in their name, but we will use the official one. An offcial image is created by the developers of the company that own that particular piece of software. Once we find a image that we are interested in, we can download it to our **Docker host**.

```bash
# Download fedora repository
> docker pull fedora
Pulling repository fedora
ded7cd95e059: Download complete
48ecf305d2cf: Download complete
Status: Downloaded newer image for fedora:latest

# List the available images on our Docker host
> docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             VIRTUAL SIZE
fedora              22                  ded7cd95e059        8 weeks ago         186.5 MB
fedora              latest              ded7cd95e059        8 weeks ago         186.5 MB
```

Listing the available images, we get image **ID: ded7cd95e059** that was tagged with different names: **latest** and **22**. In **Docker** one can tag a particular image with different tags so that they can be easily referred to. There is no easy way to list the available tags for a particular repository. The first option is to go to the [**Docker Hub**](https://registry.hub.docker.com/search?q=library) web page and search for the repository and there you will also find the list of available tags.

The second option is to use the **REST API** of **Docker Hub** to get this list:

```bash
# Get list of tags for the fedora repository
~ [root@docker-0 ~]# curl -s https://registry.hub.docker.com//v1/repositories/ubuntu/tags | python -mjson.tool | grep name
"name": "10.04"
"name": "12.10"
"name": "13.04"
"name": "13.10"
"name": "14.04.1"
"name": "14.04.2"
"name": "14.04.3"
"name": "14.10"
"name": "15.04"
"name": "lucid"
```

What happens here is that we request the list of tags for the **ubuntu** repository by doing a simple **HTTP GET** request and then we format nicely the **JSON** response after which we display only the field that we are interested in i.e. the name of the tag. After downloading the desired **Docker image**, we can fire-up our first **Docker container**.
Note that **Docker Hub** is currently migrating to v2 of the REST API and there is some functionality which migth not work as expected.

### Starting a Docker container

To start a new **Docker container** we need to use the **Docker client** command and instruct the **Docker daemon** to take the necessary steps to fire-up our container. At this point we can see how the building blocks of Docker containers fit together. In order to have a running container, we need to have an image which will be used as the underlying "operating system" for the processes that run inside the container. In our case this is the **fedora:22** image. Another important aspect is that in order to start a **Docker container** we need to tell it what process/processes to run inside the container.

```bash
# Start new Docker container using fedora:22 as the base image
> docker run -it --rm fedora:22 /bin/bash
```

A **Docker container** is in running mode as long as there is at least one process running inside it. Therefore, we can not have a running **Docker container** without a running process inside it. An analogy can be made between the **init** process in a Linux operating system and the initial process that is used when we start a container. This initial process is the parent of all other processes that are started inside the container. As soon as this initial process is killed the entire **Docker container** is stopped - and also all other processes running inside the container.

Once you execute the `docker run` command you will notice that the prompt has changed and it should look similar to the following:

```bash
[root@755ede9cd3d7 /]# cat /etc/redhat-release
Fedora release 22 (Twenty Two)
[root@755ede9cd3d7 /]# yum install procps
....
[root@755ede9cd3d7 /]# ps aux
USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
root         1  0.0  0.3  12676  4032 ?        Ss   12:08   0:00 /bin/bash
root        36  0.0  0.3  47964  3484 ?        R+   12:11   0:00 ps aux
```

After executing the `docker run` command with the `-it` flags, basically requesting a console to the container, we end up with a prompt inside the container. The hostname of the container in this case `755ede9cd3d7` is generated by the **Docker daemon** but can also be enforced when starting the container by using the `-h` flag. Now, the question might be how do we really know we are inside the container and that the container is using the image that we requested initially? There are several ways to answer this:

* First we do a `cat /etc/redhat-release` and we can see that the operating system is **Fedora 22**. Remember that the host operating system is a **CentOS7** machine.
* We can open another terminal on the host machine and execute the `docker ps` command which will list the running **Docker containers**.

```bash
>docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
755ede9cd3d7        fedora:22           "/bin/bash"         24 hours ago        Up 9 hours                              loving_sammet
```

* We can also list the installed packages using `rpm -qa` in the terminal window of the container.

Another interesting point would be to get the list of running processes inside the container. In order to do this we need to install the `procps` package which is missing from the default **Fedora 22** image. At this point you will see that the `ps aux` command will output only two running processes:

* The initial process that started the container (in the listing above the one with PID 1)
* The process corresponding to the command that we just executed (PID 36).

Compare the output of this command with the output you get by executing the same command on the host operating system. One fundamental difference between a container and a virtual machine, is that the container can leverage the capabilities of the host kernel without the need to spawn a fully running operating system for the processes that run inside it. What this means is that you can run a simple process inside the container without any additional overhead.

### Tracking container changes

What we have done so far is started a container using a **fedora:22** image and installed the `procps` package on top. Therefore the image that the container is running on is different from the one we started with. In order to track the exact changes done on the `root` filesystem of the container we can use the following command:

```bash
> docker diff 755ede9cd3d7
A /.wh..wh.plnk/887.656437
C /etc
C /etc/ld.so.cache
C /etc/pki
C /etc/pki/nssdb
C /etc/pki/nssdb/cert9.db
C /etc/pki/nssdb/key4.db
C /root
A /root/.pki
A /root/.pki/nssdb
C /tmp
C /usr
C /usr/bin
A /usr/bin/free
A /usr/bin/pgrep
A /usr/bin/pidof
A /usr/bin/pkill
A /usr/bin/pmap
A /usr/bin/ps
A /usr/bin/pwdx
...
```

This outputs the list of newly added or modified files. All these changes are not saved yet anywhere, so when we kill the container we would also loose them. What would be a desirable scenario is to create a new image from the currently running container, so that the next time we start a container we won't have to install again the `procps` package. To achieve this, we use another **Docker** command that snapshots the current container and creates an image that we can reuse in the future.

```bash
# Take snapshot and create new image
> docker commit 755ede9cd3d7 esindril/fedora_with_procps
8ec1d8c4632c04cfe0422089a7afafc4a2d24a894ef4bd08774272f9e018b047

# List the currently available images
> docker images
REPOSITORY                    TAG                 IMAGE ID            CREATED             VIRTUAL SIZE
esindril/fedora_with_procps   latest              8ec1d8c4632c        7 seconds ago       317 MB
fedora                        22                  ded7cd95e059        8 weeks ago         186.5 MB
fedora                        latest              ded7cd95e059        8 weeks ago         186.5 MB
```

### Working with volumes

The root filesystem which represents the image the container runs on is a Union File System in the sense that it can record the modifications done to the files as we've seen earlier by using the **docker diff** command. This type of filesystem incurs an overhead which might not be acceptable in some cases. Therefore, we would like to avoid storing data which is frequently accessed or modified on the root filesystem. In order to achieve this we need to use data volumes. Data volumes persist data, even when containers are destroyed. They can also be shared and reused among containers. To achieve this we'll create a directory on the **Docker host** and mount it inside the container at start up.

```bash
# Create a directory on the Docker host
> mkdir /tmp/data_store
> cp /etc/passwd /tmp/data_store/test.dat
> docker run -it --rm -v /tmp/data_store:/mnt/data fedora:22 /bin/bash
# Now we are inside the container
[root@3be028e0585b /]# ls -lrt /mnt/data/
total 4
-rw-r--r-- 1 1000 ftp 213 Jul 24 13:12 test.dat
[root@3be028e0585b /]# touch /mnt/data/new_test.dat
# Execute the following command on the Docker host
> ls -lrt /tmp/data/store
ls -lrt /tmp/data_store/
total 4
-rw-r--r--    1 docker   staff          213 Jul 24 13:12 test.dat
-rw-r--r--    1 root     root             0 Jul 24 13:15 new_test.dat
```

Any data that was previously in the directory will be visible inside the container and any new data written from inside the container will be visible on the **Docker host**.

**Note**

If SElinux is enforced on the **Docker host** machine, then you need to add the proper label to the directory which is going to be mounted inside the container so that processes running inside the container can access it: `chcon -Rt svirt_sandbox_file_t /tmp/data_store/`.

### Exposing ports

In order to make services running inside containers accessible from the exterior we need to have a mechanism to expose ports. This can be achieved by passing the `-p` option when starting the container.

```bash
> docker run -t --rm -p 80:8080 fedora:22 /bin/bash
...
> docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS                  NAMES
edcb542cfaff        fedora:22           "/bin/bash"         23 seconds ago      Up 21 seconds       0.0.0.0:80->8080/tcp   clever_mestorf
```

The `docker ps` command lists the status of the running containers and we can see in the **PORTS** section the mapping that we just added. Therefore, any traffic that comes to the **Docker host** machine on port 80 is then forwarded to our container on port 8080.

### Linking containers

When running a more complex service, exposing ports might just not be enough. Sometimes, we need to run several daemons in different containers and these daemons need to interact between each other. Among the many actions that the **Docker daemon** takes when starting a container, it is also responsible for allocating IP addresses to containers. We can retrieve the IP address of a running container by using the **docker inspect** command:

```bash
> docker inspect edcb542cfaff | grep -i ipaddress
  "IPAddress": "172.17.0.9",
  "SecondaryIPAddresses": null,
```

Once we start a container the **Docker daemon** is aware of the mapping between the container name and its IP address. Let's assume now, that we would like to start a second container that needs to contact over the network a service running inside the first container. All containers are allocated IP addresses from the same subnetwork. Therefore, if we know the IP address for the first container we can reach it directly from the second one.

But this approach is not practical, since a process running inside the second container can not discover on its own the IP address of the first running container. This where we can use the `--link` option for the **docker run** command to link two containers. The link option takes the name of a running container and the **Docker daemon** will insert the necessary information in the `/etc/hosts` file of the new container so that it knows the IP address of the containers that it is linked agains. The **Docker daemon** can do this, since it has a global overview of all the running containers on the local machine.

```bash
# Start new container and link against the first one
> docker run -it --rm --link clever_mestorf ubuntu /bin/bash
root@a4bcc3dcd0c1:/# cat /etc/hosts
172.17.0.11     a4bcc3dcd0c1
127.0.0.1       localhost
::1     localhost ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
172.17.0.9      jolly_davinci edcb542cfaff
root@a4bcc3dcd0c1:/# ping jolly_davinci
PING jolly_davinci (172.17.0.9) 56(84) bytes of data.
64 bytes from jolly_davinci (172.17.0.9): icmp_seq=1 ttl=64 time=0.190 ms
64 bytes from jolly_davinci (172.17.0.9): icmp_seq=2 ttl=64 time=0.074 ms
```

## [Exercise Python HTTP server](#exercise_python)

Create a **Docker container** that runs a simple HTTP Python server, serving files from a directory. The directory must contain at least an `index.html` file with the following contents:

```html
<!DOCTYPE html>
<html lang=en>
<head>
  <meta charset=utf-8>
  <title>Sample HTML index.html for GridKA 2017 school</title>
</head>
<body>
  <p>Congratulations, your simple HTTP server is working!</p>
</body>
</html>
```

* The easiest way to run a Python HTTP server is to use the built-in SimpleHTTPServer which comes by default with python2. You can start it by executing the following command:
`python -m SimpleHTTPServer <port_number>`. The `<port_number>` option specifies the port on which the server will listen for incoming requests. Your container will have to run the HTTP server on port 8001 and it will also need to be accessible from the **Docker host** machine on the usual HTTP port 80.

* The data that the server will deliver to the clients will sit on the **Docker host** machine in `/tmp/HTTP_data` where you will need to create the `index.html` file. This data needs to be accessible to the python server inside the container from the `/mnt/data/` directory. One important aspect to keep in mind is that the SimpleHTTPServer will serve files from the directory in which it was started. If SElinux is enforced on the **Docker host** machine, then you need to add the proper label to the directory which is going to be mounted inside the container and therefore accesed by the Python server by doing: `chcon -Rt svirt_sandbox_file_t /tmp/HTTP_data`.

* When creating the **Docker container** keep in mind that we would like to have the simplest configuration that would deliver the required service. You could start from a base **fedora** or **ubuntu** image and then install the rest of the required packages but in this case the only thing we need is Python. There might already be a **Docker** image in the **Docker Hub** which provides exactly what we need.

* In order to test that the HTTP server is working properly, you can use the text-based browser `lynx` to access the `index.html` page.

In the first part of the assignment you need to be able to access the web page from the **Docker host** machine.

In the second part of the assignment you need to access the same information from another container which is running on an **ubuntu** image. You need to make this second container aware of the address where the first container runs.

## [Linux namespaces and cgroups at work](#namespaces_and_cgroups)

The two fundamental technologies underlying containers are: namespaces and cgroups. In this part of the tutorial we will see exactly how each of them provides the necessary isolation and additional functionality that make containers such a big success. Let's use a different type of operating system for this exercise - we'll use an **ubuntu** image.

```bash
> docker pull
docker pull ubuntu
Pulling repository ubuntu
d2a0ecffe6fa: Download complete
83e4dde6b9cf: Download complete
b670fb0c7ecd: Download complete
29460ac93442: Download complete
Status: Downloaded newer image for ubuntu:latest
> docker images
REPOSITORY                    TAG                 IMAGE ID            CREATED             VIRTUAL SIZE
esindril/fedora_with_procps   latest              8ec1d8c4632c        2 days ago          317 MB
ubuntu                        latest              d2a0ecffe6fa        2 weeks ago         188.4 MB
ubuntu                        trusty-20150630     d2a0ecffe6fa        2 weeks ago         188.4 MB
ubuntu                        trusty              d2a0ecffe6fa        2 weeks ago         188.4 MB
ubuntu                        14.04               d2a0ecffe6fa        2 weeks ago         188.4 MB
ubuntu                        14.04.2             d2a0ecffe6fa        2 weeks ago         188.4 MB
fedora                        20                  1b48ab88a33e        2 weeks ago         290.6 MB
fedora                        heisenbug           1b48ab88a33e        2 weeks ago         290.6 MB
fedora                        latest              ded7cd95e059        8 weeks ago         186.5 MB
fedora                        22                  ded7cd95e059        8 weeks ago         186.5 MB
```

We'll start an **ubuntu:latest** container for which we enforce a particular hostname. Once inside the container we'll run 4 processes (or 2 depending on your VM) in parallel so that they will eat up all the CPU on our **Docker host** machine.

```bash
> hostname -f
<your_vm_hostname>
> docker run -it --rm -h girdka.de ubuntu:latest /bin/bash
root@gridka:/# hostname -f
gridka.de
```

So, we managed to start a container in a different UTS namespace. From the `man namespace`: UTS(Unix Time-Sharing) namespaces provide isolation of two system identifiers: the hostname and the NIS(Network Service Information) domain name. In conclusion, we can assign whatever hostname values we desire to the containers in such a way that the processes running inside, believe they are running on a completely different host than the **Docker host** machine.

Now, we'll start a pretty useless process inside the newly created container that has the desirable (or undesirable in general) property of being CPU intensive:

```bash
root@gridka:/# dd if=/dev/zero of=/dev/null &
[1] 16
root@gridka:/# ps aux
USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
root         1  0.0  0.3  18176  3400 ?        Ss   13:19   0:00 /bin/bash
root        16  107  0.0   4376   708 ?        R    13:33   0:02 dd if=/dev/zero of=/dev/null
root        17  0.0  0.2  15572  2112 ?        R+   13:33   0:00 ps aux
root@gridka:/# top -d 1
Tasks:   3 total,   2 running,   1 sleeping,   0 stopped,   0 zombie
%Cpu(s): 10.2 us, 15.2 sy,  0.0 ni, 74.6 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
KiB Mem:   1023864 total,   938376 used,    85488 free,    38868 buffers
KiB Swap:  1212064 total,      392 used,  1211672 free.   738948 cached Mem

PID USER      PR  NI    VIRT    RES    SHR S  %CPU %MEM     TIME+ COMMAND
16 root      20   0    4376    708    628 R  99.7  0.1   0:41.53 dd
1 root      20   0   18176   3400   2920 S   0.0  0.3   0:00.13 bash
   18 root      20   0   19880   2520   2152 R   0.0  0.2   0:00.00 top
```

So, we have our `dd` process running inside the container and taking one CPU core. The process id for the `dd` program is 16 in the above printout. If we now open a new terminal to the **Docker host** machine and search for the `dd` process we get the following:

```bash
# Note this command is run on the Docker host machine i.e. your inital VM machine
> ps aux | grep "dd"
ps aux | grep dd
10483 root     dd if=/dev/zero of=/dev/null
10541 docker   grep dd
```

At this point, one can see the PID namespace at work. We have our process running inside the container which has PID 16, but the same process is also visible on the **Docker host** machine with a totally different PID number (in this case 10483). Therefore, PID namespaces further enforce the claim on isolation, as a process from inside a container is not aware of any other processes besides the ones that run in the same container as itself.

The namespaces and cgroups functionality are both embedded in the kernel code. In order to interact with the cgroups we need some user-space tools capable of doing this. All the necessary binaries are provided by the `libcgroup-tools` package which you need to install on your **Docker host** machine if it is not already installed. Cgroups are exposed to the user through a **virtual filesystem**. Therefore, cgroups can also be explored as a normal hierarchy of directories and files without the need for any special tools.

```bash
# Check if cgroups are mounted on your system
> df -h | grep cgroup
tmpfs           3.9G     0  3.9G   0% /sys/fs/cgroup
# Install the user space tools necessary to interact with cgroups
> yum install -y libcgroup-tools
...
> rpm -ql libcgroup-tools-0.41-8.el7.x86_64 | grep bin
/usr/bin/cgclassify
/usr/bin/cgcreate
/usr/bin/cgdelete
/usr/bin/cgexec
/usr/bin/cgget
/usr/bin/cgset
/usr/bin/cgsnapshot
/usr/bin/lscgroup
/usr/bin/lssubsys
/usr/sbin/cgclear
/usr/sbin/cgconfigparser
/usr/sbin/cgrulesengd
```

While there are quite a few commands that deal with cgroups, we will only use a subset of them to achive our tasks in this tutorial. Feel free to have a look at the rest of them and think how they might be useful in different scenarios. Let's start by first listing the available subsystems on our machine which represent the categories of resources that cgroups can control the access to.

```bash
> lssubsys -am
cpuset /sys/fs/cgroup/cpuset
cpu,cpuacct /sys/fs/cgroup/cpu,cpuacct
memory /sys/fs/cgroup/memory
devices /sys/fs/cgroup/devices
freezer /sys/fs/cgroup/freezer
net_cls /sys/fs/cgroup/net_cls
blkio /sys/fs/cgroup/blkio
perf_event /sys/fs/cgroup/perf_event
hugetlb /sys/fs/cgroup/hugetlb
```

Today, we'll focus on the **cpuset** subsystem. According to the [kernel documentation](https://www.kernel.org/doc/Documentation/cgroups/cgroups.txt), **Cpusets** constrain the CPU and Memory placement of tasks to only the resources within a task's current cpuset. These are the essential hooks required to manage dynamic job placement on large systems. Inside each subsystem, there is a nested hierarchy of cgroups that refine the access permissions of the processes to the corresponding resources (subsystems).

```bash
# List all available cgroups for all subsystems
> lscgroup
cpuset:/
cpuset:/system.slice
cpuset:/system.slice/docker-cfab633638d0002c6b76c4a054743e3695f5daf7bb0642f768d82cc33bbd1763.scope
cpu,cpuacct:/
cpu,cpuacct:/machine.slice
cpu,cpuacct:/user.slice
cpu,cpuacct:/system.slice
cpu,cpuacct:/system.slice/docker-storage-setup.service
cpu,cpuacct:/system.slice/cups.service
cpu,cpuacct:/system.slice/gdm.service
cpu,cpuacct:/system.slice/dbus.socket
cpu,cpuacct:/system.slice/brandbot.service
cpu,cpuacct:/system.slice/systemd-logind.service
cpu,cpuacct:/system.slice/systemd-ask-password-wall.service
...

# Show all cgroups for a particular subsystem (cpusets)
> lscgroup cpuset:/
lscgroup cpuset:/
cpuset:/
cpuset:/system.slice
cpuset:/system.slice/docker-cfab633638d0002c6b76c4a054743e3695f5daf7bb0642f768d82cc33bbd1763.scope

# List the running containers on this Docker host machine
docker ps --no-trunc=true
CONTAINER ID                                                       IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
cfab633638d0002c6b76c4a054743e3695f5daf7bb0642f768d82cc33bbd1763   ubuntu:latest       "/bin/bash"         9 minutes ago       Up 9 minutes                            fervent_elion
```

You will notice that the hash value of the running container matches the hash value of the cgroup created inside `cpuset:/system.slice/`. Therefore, any process that runs inside this container belongs to the cgroup `/system.slice/docker-cfab633638d0002c6b76c4a054743e3695f5daf7bb0642f768d82cc33bbd1763.scope` and is constrained by the configuration of this cgroup. Let's see how this configuration looks like:

```bash
> cgget -g cpuset:/system.slice/docker-cfab633638d0002c6b76c4a054743e3695f5daf7bb0642f768d82cc33bbd1763.scope
/system.slice/docker-cfab633638d0002c6b76c4a054743e3695f5daf7bb0642f768d82cc33bbd1763.scope:
cpuset.memory_spread_slab: 0
cpuset.memory_spread_page: 0
cpuset.memory_pressure: 0
cpuset.memory_migrate: 0
cpuset.sched_relax_domain_level: -1
cpuset.sched_load_balance: 1
cpuset.mem_hardwall: 0
cpuset.mem_exclusive: 0
cpuset.cpu_exclusive: 0
cpuset.mems: 0
cpuset.cpus: 0-3

# Retrieve the cpus in this cpuset for this cgroup directly
> cgget -r cpuset.cpus /system.slice/docker-cfab633638d0002c6b76c4a054743e3695f5daf7bb0642f768d82cc33bbd1763.scope
/system.slice/docker-cfab633638d0002c6b76c4a054743e3695f5daf7bb0642f768d82cc33bbd1763.scope:
cpuset.cpus: 0-3
```

What we're interested in from this listing is the `cpuset.cpus` which represents the identifiers of the CPUs nodes on which the processes running inside this cgroup can be scheduled. Therefore, at this point any process can access any of the available CPUs (0, 1, 2 or 3). Now, we go back to our already running container and add a few more CPU intenssive processes forcing all our CPUs to run at 100%.

```bash
# Start as many processes as the number of CPUs available (look insde /proc/cpuinfo)
root@gridka:/# dd if=/dev/zero of=/dev/null &
root@gridka:/# dd if=/dev/zero of=/dev/null &
root@gridka:/# dd if=/dev/zero of=/dev/null &
root@gridka:/# dd if=/dev/zero of=/dev/null &

# Look at the resource consumption (press 1 after running `top` to see individual CPUs)
root@gridka:/# top -d 1
Tasks:   6 total,   5 running,   1 sleeping,   0 stopped,   0 zombie
%Cpu0  : 25.0 us, 68.8 sy,  0.0 ni,  0.0 id,  0.0 wa,  0.0 hi,  6.2 si,  0.0 st
%Cpu1  : 50.0 us, 50.0 sy,  0.0 ni,  0.0 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
%Cpu2  : 25.0 us, 75.0 sy,  0.0 ni,  0.0 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
%Cpu3  : 37.5 us, 62.5 sy,  0.0 ni,  0.0 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
KiB Mem:   1023864 total,   809012 used,   214852 free,    40212 buffers
KiB Swap:  1212064 total,        0 used,  1212064 free.   615416 cached Mem

PID USER      PR  NI    VIRT    RES    SHR S  %CPU %MEM     TIME+ COMMAND
43 root      20   0    4376    684    604 R 100.0  0.1   0:04.60 dd
42 root      20   0    4376    728    652 R  97.5  0.1   0:05.14 dd
44 root      20   0    4376    700    620 R  97.5  0.1   0:04.06 dd
45 root      20   0    4376    728    648 R  97.5  0.1   0:03.47 dd
1 root      20   0   18176   3400   2920 S   0.0  0.3   0:00.21 bash
46 root      20   0   19880   2504   2136 R   0.0  0.2   0:00.00 top
```

In a real world scenario, this can be an application which is missbehaving and is taking way too much CPU than it should normally do. This in its turn affects the performance of all other services or applications running on the same host. What can we do about it? Well, this is the point where cgroups become useful. Let's try and reduce the number of CPUs available to processes running inside our container.

```bash
# Modify the cgroup to only use 2 of the 4 CPUs available
> cgset -r cpuset.cpus="0-1" /system.slice/docker-cfab633638d0002c6b76c4a054743e3695f5daf7bb0642f768d82cc33bbd1763.scope

# Look at the resource consumption for processes runnning inside the container
root@gridka:/# top -d 1
Tasks:   6 total,   5 running,   1 sleeping,   0 stopped,   0 zombie
%Cpu0  : 19.8 us, 80.2 sy,  0.0 ni,  0.0 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
%Cpu1  : 19.0 us, 81.0 sy,  0.0 ni,  0.0 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
%Cpu2  :  0.0 us,  0.0 sy,  0.0 ni,100.0 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
%Cpu3  :  0.0 us,  0.0 sy,  0.0 ni,100.0 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
KiB Mem:   8012028 total,  6608312 used,  1403716 free,     1788 buffers
KiB Swap:        0 total,        0 used,        0 free.  5719588 cached Mem

PID USER      PR  NI    VIRT    RES    SHR S  %CPU %MEM     TIME+ COMMAND
25 root      20   0    4368    356    280 R  50.8  0.0   0:59.86 dd
26 root      20   0    4368    356    280 R  50.8  0.0   0:59.67 dd
23 root      20   0    4368    360    280 R  49.8  0.0   1:01.43 dd
24 root      20   0    4368    356    280 R  48.8  0.0   1:00.37 dd
1 root      20   0   18168   1984   1508 S   0.0  0.0   0:00.04 bash
28 root      20   0   19872   1412   1044 R   0.0  0.0   0:00.00 top
```

Looking at the resource consumption inside our container, we see that although we still have the 4 CPU intenssive processes running, only 2 CPU are running at 100%. The rest of the CPUs are idle and therefore available for scheduling other processes running on the same machine. One can also see the time distribution for each process on the CPUs - it is roughly at 50% therefore, the 4 processes are splitting evenly the 2 available CPUs between themselves.

## [ROOT & XRootD exercise](#root_xrootd)

For the purpose of this exercise we will try to recreate a simple workflow which is very common in HEP (High Energy Physics) data analysis. The final goal will be to do some simple physics analysis on a set of data. The first part of the exercise is to set up a [**XRootD server**](http://xrootd.org) inside a **Docker container**. The XRootD project is a client-server framework used heavily in the HEP community for distributing and serving data.

### XRootD container requirements

* The base image for the container will be a Fedora 20 image.
* The hostname of the container should be **xrootd.de**.
* The container will need to make the XRootD service available to the exterior on port 1094. **Obs.:** the XRootD server runs by default on port 1094.
* The XRootD server will need to serve data from a volume mounted inside the container at `/mnt/data`. The data will reside on the **Docker host** machine in directory `/tmp/xrd_data/`.

In order to start an XRootD data server you will need to install the `xrootd` and `xrootd-client`  packages. This you can do with the default package manager `yum`. By default, the XRootD server will export the data from `/tmp/`. We would like to change this so that is exports the data from `/mnt/data`. You can do this by updating the configuration script that the XRootD daemon reads at start-up, which is located at `/etc/xrootd/xrootd-clustered.cfg` (look for the `all.export` directive). Once the configuration changes are done, you can start the service using the following command `service xrootd start`.

**Note**

* The XRootD server runs under the user `xrootd` and it needs to own the directory from which it will serve the data i.e. `/mnt/data`.
* In order to test that the server works fine, one can do a test transfer: copy the `/etc/passwd` file to the server using the XRootD copy command:

```bash
> xrdcp -f -d 1 /etc/passwd root://localhost:1094//mnt/data/test.dat
```

Inspecting the contents of the `/mnt/data/` directory should reveal the new `test.data` file. At this point you have a running **XRootD server** which can store and deliver data to its clients. Don't forget to test that the XRootD server is also available from ouside the container by doing another transfer from the **Docker host** machine. Furthermore, verify that the data is actually present in `/tmp/xrd_data` on the **Docker host** machine.

#### ROOT container requirements

In the second part of the exercise you are required to set up the [**ROOT**](https://root.cern.ch/) framework for data analysis in a different **Docker container**. You can read more about the **ROOT** project on their web site, but just so that you know this is the most widely used tool for performing data analysis in the High Energy Physics community.

The container should meet the following requirements:

* The base image for the container will be **CentOS7**.
* The container's name should be **root_application**.
* You will need to install the following packages that the **ROOT** framework depends on:

```bash
wget gcc gcc-c++ epel-release net-tools vim xrootd-client libSM libX11 libXext libXpm libXft libpng libjpeg-turbo
```

* To get the **ROOT** binary release just download it from this location: http://root.cern.ch/download/root_v6.04.02.Linux-cc7-x86_64-gcc4.8.tar.gz into `/mnt/`.
* Once the archive is downloaded, extract it and setup the environment by sourcing the file: `source /mnt/root/bin/thisroot.sh`.
* At this point you should be able to launch **ROOT** in your container by typing the command: `root -b`
* To exit the ROOT console, type: `.q`
* Once you have everything installed, promote the new container to an image called `<your_user_name>/root_img`.

After completing the basic setup of the containers, you can move on the the following tasks:

* Check that you can successfully transfer files from the `root_application` container to the **XRootD server** that you set up in the first part of the exercise.

* Create a new file `/mnt/hist.cpp` in the running `root_application` container, with the following contents:

```c++
#include "TFile.h"
#include "TH1F.h"
#include "TRandom.h"
#include "TRandom3.h"
#include <iostream>

int main(int argc, char* argv[])
{
  gRandom = new TRandom3(0);
  std::cout << "Generating histogram" << std::endl;
  // Try to save the file in the XRootD server running on
  // root://localhost:1094//
  TFile* f = TFile::Open("histo.root","new");
  TH1F h1("hgaus","GridKa - Histogram from a gaussian ",100,-3,3);
  h1.FillRandom("gaus",10000);
  h1.Write();
  delete f;
  return 0;
}
```

* Compile and build the file using the following command: `g++ -o run_hist hist.cpp $(root-config --cflags --glibs) ` which produces the `run_hist` executable. This program creates a histogram using a Gaussian distribution and saves the file in the same directory from where the executable was run.

* Modify the program such that the histogram file is saved in the **XRootD** server that you set up in the first part of the exercise. Check that a new `histo.root` was successfully created in the directory used by the **XRootD** server to save files.

* Automate the creation of the two containers using **Dockerfiles** and the `docker build` command.

**Extra**

In order to visualise the generated histogram, we need to be able to access the X11 server from inside the `root_application` container. There are several additional steps that you need to take in order to have this setup working:

* Fist check on the **Docker host** machine that you have X forwarding enabled by executing the command: `xclock`. If everything works fine, you should see a clock displayed on your screen.
* You need to allow the processes that run inside the container to access the X server special directory. For this you need to execute, the usual by now: `chcon -Rt svirt_sandbox_file_t /tmp/.X11-unix`
* When starting the `root_application` container you need to pass the `--net=host` option, so that the container and the host share the same UTS namespace - and can access the same socket to the X server.
* One last thing you need to take care of, is forwarding the .Xauthority file inside the container in read-only mode by doing: `-v /root/.Xauthority:/root/.Xauthority:ro`.
* Investigate how you can use the TBrowser() **ROOT** object to visualise the histograms - both the one locally saved and the one in the **XRootD** server.
